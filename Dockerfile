FROM python:3.6 as web

COPY /requirements.txt /opt/requirements.txt
RUN pip install -r /opt/requirements.txt

COPY . /opt/
RUN pip install -e /opt/

EXPOSE 8000

WORKDIR /opt/server
CMD python manage.py runserver 0:8000


## docs
FROM web as docs

RUN pip install sphinx sphinx-autobuild

EXPOSE 8080

WORKDIR /opt/docs
CMD sphinx-autobuild -H 0.0.0.0 -p 8080 -b html ./ _build/html