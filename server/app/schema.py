import graphene
from django.apps import apps
from graphene_django.types import DjangoObjectTypeOptions

from graphene_django_framework.fields import (ModelPageListField, ModelField, ModelListField, ModelFilterField,
                                              ModelFilterPageField)
from graphene_django_framework.types import ModelObjectType
from graphene_django_framework.decorators import needs_permission, only_allow

from .filters import UserFilter


class BaseModelAdminInterface(graphene.Interface):
    @classmethod
    def __init_subclass_with_meta__(cls, _meta=None, **options):
        if not _meta:
            _meta = DjangoObjectTypeOptions(cls)
        _meta.paginator = 'THIS IS THE PAGE BEFORE'

        super(BaseModelAdminInterface, cls).__init_subclass_with_meta__(_meta=_meta, **options)


class ModelAdminInterface(BaseModelAdminInterface):
    model_name = graphene.String()


class PermissionType(ModelObjectType):

    class Meta:
        model = apps.get_model('auth', 'Permission')
        only_fields = ('name', 'codename')
        filter_fields = {'content_type': ['exact']}
        interfaces = (ModelAdminInterface, )
        perms = ('auth.view_user', 'auth.view_permission')


class GroupType(ModelObjectType):

    class Meta:
        model = apps.get_model('auth', 'Group')
        interfaces = (ModelAdminInterface,)


class UserType(ModelObjectType):

    @classmethod
    def get_qs(cls, resolver, root, info, **args):
        return cls._meta.model._default_manager.all()

    class Meta:
        model = apps.get_model('auth', 'User')
        filter_fields = {'username': ['icontains', 'exact'],
                         'first_name': ['icontains', 'exact'],
                         'is_staff': ['exact'],
                         'is_active': ['exact'],
                         'is_superuser': ['exact']
                         }
        interfaces = (ModelAdminInterface,)


def asdf_user_resolver(*args, **kwargs):
    return []


def users_custom_resolver(*args, **kwargs):
    return []


class Query(graphene.ObjectType):
    hello = graphene.String(name=graphene.String(default_value="stranger"))
    user_parent = ModelField(UserType)
    user_default = ModelField(UserType, required=True)
    group = graphene.Field(GroupType)
    users = graphene.List(UserType, username=graphene.String())
    user_list = ModelListField(UserType, required=True, resolver=asdf_user_resolver)
    all_users = ModelPageListField(UserType, required=True)
    all_users_empty_list = ModelPageListField(UserType, required=True, resolver=users_custom_resolver)

    filter_users = ModelFilterField(UserType, required=True)
    filter_users_custom_filterset = ModelFilterField(UserType, filter_set_class=UserFilter, required=True)
    filter_paginate_users = ModelFilterPageField(UserType, required=True)

    permissions = ModelListField(PermissionType)
    filter_perms = ModelFilterField(PermissionType)

    # Default resolver
    users_default = ModelListField(UserType, required=True)
    # Resolver defined on Query class
    users_parent = ModelListField(UserType)
    # Custom resolver passed to Field
    users_custom = ModelListField(UserType, resolver=users_custom_resolver)

    users_perm_check = ModelFilterField(UserType, required=True)

    def resolve_hello(self, info, name):
        return 'Hello %s' % name

    @needs_permission('auth.view_user')
    def resolve_user_parent(self, info, id=None):
        return apps.get_model('auth', 'User').objects.get(id=id)

    def resolve_users_parent(self, info, **kwargs):
        return []

    def resolve_users(self, info, **kwargs):
        return [UserType(**{'username': 'ONE', 'first_name': 'FNAME', 'last_name': 'LNAME'}),
                UserType(**{'username': 'TWO', 'first_name': 'FNAME', 'last_name': 'LNAME'}),
                UserType(**{'username': 'THREE', 'first_name': 'FNAME', 'last_name': 'LNAME'}),
                UserType(**{'username': 'FOUR', 'first_name': 'FNAME', 'last_name': 'LNAME'})]

    @only_allow(superuser=True)
    def resolve_all_users(self, info, username=None, **kwargs):
        return apps.get_model('auth', 'User').objects.all()

    @only_allow(staff=True)
    @needs_permission('auth.view_permission')
    def resolve_permissions(self, info, **args):
        return apps.get_model('auth.Permission').objects.all()

    @only_allow(staff=True)
    def resolve_users_perm_check(self, info, **kwargs):
        User = apps.get_model('auth.User')

        if info.context.user.has_perm('auth.view_user'):
            return User.objects.all()

        return None


schema = graphene.Schema(query=Query)
