
from django.apps import apps
from django.test import TestCase, RequestFactory
from graphene.test import Client

from ..schema import schema


class ModelFilterFieldTestCase(TestCase):
    """ Tests for ModelFilterField. """

    @classmethod
    def setUpTestData(cls):
        super(ModelFilterFieldTestCase, cls).setUpTestData()

        User = apps.get_model('auth.User')

        cls.test_client = Client(schema=schema)
        cls.test_request = RequestFactory()
        cls.test_request.user = User.objects.create(username='superduperuser', is_superuser=True)

        cls.test_user_1 = User.objects.create(username='test_1', is_staff=True)
        cls.test_user_2 = User.objects.create(username='test_2', is_staff=True)
        cls.test_user_3 = User.objects.create(username='test_3', is_staff=True)

        cls.test_user_4 = User.objects.create(username='test_4', first_name='James', is_staff=False)
        cls.test_user_5 = User.objects.create(username='test_5', first_name='James', is_staff=False)

    def test_filter_field(self):
        """ ModelFilterField will return a list of type instances that match certain criteria. """
        result = self.test_client.execute(''' 
        { filterUsers(firstName:"James") {
            username
        }}
        ''', context=self.test_request)

        expected = {
            "data": {
                "filterUsers": [
                    {
                        "username": "{}".format(self.test_user_4)
                    },
                    {
                        "username": "{}".format(self.test_user_5)
                    }
                ]
            }
        }

        self.assertEqual(len(result['data']['filterUsers']), 2)
        self.assertEqual(result['data']['filterUsers'],
                         expected['data']['filterUsers'])

    def test_filter_field_with_filterset(self):
        """ ModelFilterField will return a list of type instances that match certain criteria.
            It will use a passed FilterSet when filtering the list.
        """
        result = self.test_client.execute(''' 
        { filterUsersCustomFilterset(isStaff:true) {
            username
        }}
        ''', context=self.test_request)

        expected = {
            "data": {
                "filterUsersCustomFilterset": [
                    {
                        "username": "{}".format(self.test_user_1)
                    },
                    {
                        "username": "{}".format(self.test_user_2)
                    },
                    {
                        "username": "{}".format(self.test_user_3)
                    }
                ]
            }
        }

        self.assertEqual(len(result['data']['filterUsersCustomFilterset']), 3)
        self.assertEqual(result['data']['filterUsersCustomFilterset'],
                         expected['data']['filterUsersCustomFilterset'])

    def test_filter_field_resolver_returns_none(self):
        """ ModelFilterField will return an empty list for custom resolvers that return None, rather than return
            all model instances.
        """
        User = apps.get_model('auth.User')
        test_request = RequestFactory()
        test_request.user = User.objects.create(is_staff=True)

        result = self.test_client.execute('''
        { usersPermCheck(isStaff:true) {
            id
        }}
        ''', context=test_request)

        expected = {
            "data": {
                "usersPermCheck": []
            }
        }

        self.assertEqual(result['data']['usersPermCheck'], expected['data']['usersPermCheck'])


class ModelPageListFieldTestCase(TestCase):
    """ Tests for ModelPageListField. """

    @classmethod
    def setUpTestData(cls):
        super(ModelPageListFieldTestCase, cls).setUpTestData()

        User = apps.get_model('auth.User')
        cls.test_superuser = User.objects.create(username='superduperuser', is_superuser=True)

        cls.test_client = Client(schema=schema)
        cls.test_request = RequestFactory()
        cls.test_request.user = cls.test_superuser

        cls.test_user_1 = User.objects.create(username='test_1', is_staff=True)
        cls.test_user_2 = User.objects.create(username='test_2', is_staff=True)
        cls.test_user_3 = User.objects.create(username='test_3', is_staff=True)
        cls.test_user_4 = User.objects.create(username='test_4')
        cls.test_user_5 = User.objects.create(username='test_5')

    def test_page_list_field(self):
        """ ModelPageListField will return a list of type instances. """
        result = self.test_client.execute('''
        { allUsers {
            pageInfo {
              objectCount
              numPages
              hasNext
              hasPrevious
              hasOtherPages
              nextPageNumber
              previousPageNumber
              startIndex
              endIndex
            }
            objectList {
                username
                isStaff
            }
        }}
        ''', context=self.test_request)

        # Assert page info has the correct pagination information
        self.assertEqual(result['data']['allUsers']['pageInfo']['objectCount'], 6)
        self.assertEqual(result['data']['allUsers']['pageInfo']['numPages'], 1)
        self.assertFalse(result['data']['allUsers']['pageInfo']['hasNext'])
        self.assertFalse(result['data']['allUsers']['pageInfo']['hasPrevious'])
        self.assertFalse(result['data']['allUsers']['pageInfo']['hasOtherPages'])
        self.assertIsNone(result['data']['allUsers']['pageInfo']['nextPageNumber'])
        self.assertIsNone(result['data']['allUsers']['pageInfo']['previousPageNumber'])
        self.assertEqual(result['data']['allUsers']['pageInfo']['startIndex'], 1)
        self.assertEqual(result['data']['allUsers']['pageInfo']['endIndex'], 6)

        # Assert all Users are in the object list
        self.assertIn({'username': self.test_superuser.username, 'isStaff': self.test_superuser.is_staff},
                      result['data']['allUsers']['objectList'])
        self.assertIn({'username': self.test_user_1.username, 'isStaff': self.test_user_1.is_staff},
                      result['data']['allUsers']['objectList'])
        self.assertIn({'username': self.test_user_2.username, 'isStaff': self.test_user_2.is_staff},
                      result['data']['allUsers']['objectList'])
        self.assertIn({'username': self.test_user_3.username, 'isStaff': self.test_user_3.is_staff},
                      result['data']['allUsers']['objectList'])
        self.assertIn({'username': self.test_user_4.username, 'isStaff': self.test_user_4.is_staff},
                      result['data']['allUsers']['objectList'])
        self.assertIn({'username': self.test_user_5.username, 'isStaff': self.test_user_5.is_staff},
                      result['data']['allUsers']['objectList'])


class ModelFilterPageFieldTestCase(TestCase):
    """ Tests for ModelFilterPageField. """

    @classmethod
    def setUpClass(cls):
        super(ModelFilterPageFieldTestCase, cls).setUpClass()

        User = apps.get_model('auth', 'User')

        cls.test_superuser = User.objects.create(username='superduperuser1', is_superuser=True)
        cls.test_client = Client(schema)
        cls.test_request = RequestFactory()
        cls.test_request.user = cls.test_superuser

        # These Users will show up in the filtered list
        cls.test_user_1 = User.objects.create(username='JJ123', is_staff=True)
        cls.test_user_2 = User.objects.create(username='JJbubbles', is_staff=True)
        cls.test_user_3 = User.objects.create(username='JJflippers', is_staff=True)
        cls.test_user_4 = User.objects.create(username='JJsanders', is_staff=True)

        # These users will not show up in the filtered list
        cls.test_user_5 = User.objects.create(username='spanky12')
        cls.test_user_6 = User.objects.create(username='chickennuggets1')
        cls.test_user_7 = User.objects.create(username='servedwithfries32')

    def test_filter_page_field(self):
        """ ModelFilterPageField will return a paginated list of filtered type instances. """
        result = self.test_client.execute('''
        {
          filterPaginateUsers(username_Icontains:"JJ") {
            pageInfo {
              objectCount
              numPages
              hasNext
              hasPrevious
              hasOtherPages
              nextPageNumber
              previousPageNumber
              startIndex
              endIndex
            }
            objectList {
              id
              username
              isStaff
            }
          }
        }
        ''', context=self.test_request)

        # Assert page info has the correct pagination information
        self.assertEqual(result['data']['filterPaginateUsers']['pageInfo']['objectCount'], 4)
        self.assertEqual(result['data']['filterPaginateUsers']['pageInfo']['numPages'], 1)
        self.assertFalse(result['data']['filterPaginateUsers']['pageInfo']['hasNext'])
        self.assertFalse(result['data']['filterPaginateUsers']['pageInfo']['hasPrevious'])
        self.assertFalse(result['data']['filterPaginateUsers']['pageInfo']['hasOtherPages'])
        self.assertIsNone(result['data']['filterPaginateUsers']['pageInfo']['nextPageNumber'])
        self.assertIsNone(result['data']['filterPaginateUsers']['pageInfo']['previousPageNumber'])
        self.assertEqual(result['data']['filterPaginateUsers']['pageInfo']['startIndex'], 1)
        self.assertEqual(result['data']['filterPaginateUsers']['pageInfo']['endIndex'], 4)

        # Assert Users that did match criteria are in the list
        self.assertIn({'id': str(self.test_user_1.pk),
                       'username': self.test_user_1.username,
                       'isStaff': self.test_user_1.is_staff
                       },
                      result['data']['filterPaginateUsers']['objectList']
                      )
        self.assertIn({'id': str(self.test_user_2.pk),
                       'username': self.test_user_2.username,
                       'isStaff': self.test_user_2.is_staff
                       },
                      result['data']['filterPaginateUsers']['objectList']
                      )
        self.assertIn({'id': str(self.test_user_3.pk),
                       'username': self.test_user_3.username,
                       'isStaff': self.test_user_3.is_staff
                       },
                      result['data']['filterPaginateUsers']['objectList']
                      )
        self.assertIn({'id': str(self.test_user_4.pk),
                       'username': self.test_user_4.username,
                       'isStaff': self.test_user_4.is_staff
                       },
                      result['data']['filterPaginateUsers']['objectList']
                      )

        # Assert Users that didn't match criteria aren't in the list
        self.assertNotIn({'id': str(self.test_user_5.pk),
                          'username': self.test_user_5.username,
                          'isStaff': self.test_user_5.is_staff
                          },
                         result['data']['filterPaginateUsers']['objectList']
                         )
        self.assertNotIn({'id': str(self.test_user_6.pk),
                          'username': self.test_user_6.username,
                          'isStaff': self.test_user_6.is_staff
                          },
                         result['data']['filterPaginateUsers']['objectList']
                         )
        self.assertNotIn({'id': str(self.test_user_7.pk),
                          'username': self.test_user_7.username,
                          'isStaff': self.test_user_7.is_staff
                          },
                         result['data']['filterPaginateUsers']['objectList']
                         )
