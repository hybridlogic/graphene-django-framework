
from django.apps import apps
from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory
from graphene.test import Client

from ..schema import schema
from graphene_django_framework.types import ModelObjectType


class TypeTestCase(TestCase):

    class TestModelObjectTypeWithOnePerm(ModelObjectType):
        """ ModelObjectType with one permission defined. """

        class Meta:
            model = apps.get_model('auth.User')
            perms = 'auth.change_user'

    class TestModelObjectTypeWithMultiplePerms(ModelObjectType):
        """ ModelObjectType with multiple permissions defined. """

        class Meta:
            model = apps.get_model('auth.User')
            perms = ('auth.add_user', 'auth.change_user', 'auth.delete_user', )

    class TestModelObjectTypeNoPermsRequired(ModelObjectType):
        """ ModelObjectType that will get no permissions. """

        class Meta:
            model = apps.get_model('auth.User')
            perms = ()

    class TestModelObjectTypePermsIsNone(ModelObjectType):
        """ ModelObjectType that will get default view permissions. """

        class Meta:
            model = apps.get_model('auth.User')
            perms = None

    class TestModelObjectTypePermsNotDefined(ModelObjectType):
        """ ModelObjectType that will get default view permissions. """

        class Meta:
            model = apps.get_model('auth.User')

    @classmethod
    def setUpTestData(cls):
        super(TypeTestCase, cls).setUpTestData()

        cls.user = User.objects.create(username='testuser')
        cls.test_request = RequestFactory()
        cls.test_request.user = User.objects.create(username='superduperuser', is_superuser=True)

    def test_get_perms(self):
        """ get_perms() will return perms defined in the ModelObjectType's Meta class. """
        result = self.TestModelObjectTypeWithMultiplePerms.get_perms()

        self.assertEqual(result, ('auth.add_user', 'auth.change_user', 'auth.delete_user'))

    def test_get_perms_single_perm(self):
        """ get_perms() will return a tuple of perms when a single Permission string is specified for perms in the
            ModelObjectType's Meta class. """
        result = self.TestModelObjectTypeWithOnePerm.get_perms()

        self.assertEqual(result, ('auth.change_user', ))

    def test_get_perms_no_perms_required(self):
        """ get_perms() will return None when perms is an empty tuple. """
        result = self.TestModelObjectTypeNoPermsRequired.get_perms()

        self.assertIsNone(result)

    def test_get_perms_perms_is_none(self):
        """ get_perms() will return default view perms when perms are set to None
            in the ModelObjectType's Meta class.
        """
        result = self.TestModelObjectTypePermsIsNone.get_perms()

        self.assertEqual(result, ('auth.view_user', ))

    def test_get_perms_no_perms_specified(self):
        """ get_perms() will return default view perms when no perms are
            defined in the ModelObjectType's Meta class.
        """
        result = self.TestModelObjectTypePermsNotDefined.get_perms()

        self.assertEqual(result, ('auth.view_user', ))

    def test_query_user(self):
        client = Client(schema)
        executed = client.execute('''
            {
              userParent(id: %d) {
                username
              }
              userDefault(id: %d) {
                username
              }
            }
        ''' % (self.user.id, self.user.id), context=self.test_request)
        self.assertEqual(executed,
                         {
                             "data": {
                                 "userParent": {
                                     "username": "testuser",
                                 },
                                 "userDefault": {
                                     "username": "testuser",
                                 }
                             }
                         })

    def test_query_users(self):
        client = Client(schema)
        executed = client.execute('''
            {
              allUsers {
                pageInfo {
                  objectCount
                  numPages
                  pageRange
                  hasNext
                  hasPrevious
                  hasOtherPages
                  nextPageNumber
                  previousPageNumber
                  startIndex
                  endIndex
                }
                objectList {
                  username
                  groups {
                    objectList {
                      name
                    }
                  }
                }
              }
            }
        ''', context=self.test_request)

        expected = {
            "data": {
                "allUsers": {
                    "pageInfo": {
                        "objectCount": 2,
                        "numPages": 1,
                        "pageRange": [1],
                        "hasNext": False,
                        "hasPrevious": False,
                        "hasOtherPages": False,
                        "nextPageNumber": None,
                        "previousPageNumber": None,
                        "startIndex": 1,
                        "endIndex": 2
                    },
                    "objectList": [
                        {
                            "username": "testuser",
                            "groups": {
                                "objectList": []
                            }
                        },
                        {
                            "username": "superduperuser",
                            "groups": {
                                "objectList": []
                            }
                        }
                    ]
                }
            }
        }

        self.assertEqual(executed['data']['allUsers']['pageInfo'],
                         expected['data']['allUsers']['pageInfo'])
        self.assertEqual(executed['data']['allUsers']['objectList'],
                         expected['data']['allUsers']['objectList'])

    def test_query_users_default_resolver(self):
        client = Client(schema=schema)
        result = client.execute('''
            {
              usersDefault {
                id
              }
            }
        ''', context=self.test_request)

        expected = {
            "data": {
                "usersDefault": [
                    {
                        "id": "1"
                    },
                    {
                        "id": "2"
                    }
                ]
            }
        }

        self.assertEqual(result, expected)

    def test_query_users_parent_resolver(self):
        client = Client(schema=schema)
        result = client.execute('''
            {
              usersParent {
                id
              }
            }
        ''', context=self.test_request)

        expected = {
            "data": {
                "usersParent": []
            }
        }

        self.assertEqual(result, expected)

    def test_query_users_resolver_passed_to_field(self):
        client = Client(schema=schema)
        result = client.execute('''
            {
              usersCustom {
                id
              }
            }
        ''', context=self.test_request)

        expected = {
            "data": {
                "usersCustom": []
            }
        }

        self.assertEqual(result, expected)
