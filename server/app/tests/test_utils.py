
from django.apps import apps
from django.test import TestCase
from graphene.types.resolver import get_default_resolver

from graphene_django_framework.utils import select_resolver
from graphene_django_framework.types import ModelObjectType


class SelectResolverUtilTests(TestCase):
    """ Tests for select_resolver() . """

    def custom_resolver(self, info, **args):
        """ Dummy custom resolver function. """
        pass

    class TestModelObjectType(ModelObjectType):
        """ Dummy ModelObjectType subclass. """

        class Meta:
            model = apps.get_model('auth.User')
            perms = ()

    def test_select_resolver_default_resolver_root_level(self):
        """ select_resolver() will select the get_qs() resolver defined on the passed type when
            passed the default resolver and we are at the root level of a query.
        """
        result = select_resolver(get_default_resolver(), self.TestModelObjectType, None)

        self.assertEqual(result.func, self.TestModelObjectType.get_qs)

    def test_select_resolver_default_resolver_nested_queries(self):
        """ select_resolver() will select the get_qs() resolver defined on the passed type when
            passed the default resolver and we are not at the root level of a query (nested query).
        """
        result = select_resolver(get_default_resolver(), self.TestModelObjectType, self.TestModelObjectType())

        self.assertEqual(result, get_default_resolver())

    def test_select_resolver_custom_resolver(self):
        """ select_resolver() will select the passed resolver when it is not the default resolver. """
        result = select_resolver(self.custom_resolver, self.TestModelObjectType, None)
        result_2 = select_resolver(self.custom_resolver, self.TestModelObjectType, self.TestModelObjectType())

        self.assertEqual(result, self.custom_resolver)
        self.assertEqual(result_2, self.custom_resolver)
