import django_filters
from django.apps import apps


class UserFilter(django_filters.FilterSet):

    class Meta:
        model = apps.get_model('auth.User')
        fields = {'username': ['icontains', 'exact'],
                  'first_name': ['icontains', 'exact'],
                  'last_name': ['icontains', 'exact'],
                  'is_staff': ['exact'],
                  'is_active': ['exact'],
                  }
