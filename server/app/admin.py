# Register your models here.
from django.contrib.auth.models import User, Group

from graphene_django_framework import sites


class UserAdmin(sites.ModelAdmin):
    search_fields = ('first_name', 'last_name', 'username',)
    list_display = ('username',)


sites.site.register(User, UserAdmin)


class GroupAdmin(sites.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name', )


sites.site.register(Group, GroupAdmin)
