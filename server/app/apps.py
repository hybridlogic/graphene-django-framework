import django.apps.AppConfig


class AppConfig(django.apps.AppConfig):
    name = 'app'
